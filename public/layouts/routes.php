<?php
return [
    '/campaigns/create' => 'create',
    '/campaigns/view' => 'view',
    '/campaigns/update' => 'update',
    '/campaigns/delete' => 'delete',

    '/adimages/create' => 'create',
    '/adimages/view' => 'view',
    '/adimages/update' => 'update',
    '/adimages/delete' => 'delete',

    '/adcreative/create' => 'create',
    '/adcreative/view' => 'view',
    '/adcreative/update' => 'update',
    '/adcreative/delete' => 'delete',

    '/adsets/create' => 'create',
    '/adsets/view' => 'view',
    '/adsets/update' => 'update',
    '/adsets/delete' => 'delete',

    '/ads/create' => 'create',
    '/ads/view' => 'view',
    '/ads/update' => 'update',
    '/ads/delete' => 'delete',
];