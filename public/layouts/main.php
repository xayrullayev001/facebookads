<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Facebook Ads Api</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
</head>
<body>
<?php
require ROOT_PATH . '/public/blocks/header.php';
$routes = require ROOT_PATH . '/public/layouts/routes.php';


$uri = $routePath = $_SERVER['REQUEST_URI'];
$folder  = $page = "";
if (strpos($uri, "?") > 0) {
    $query = str_split($uri, strpos($uri, "?"));
    $queryParam = $query[1];
    $path = explode("/", $query[0]);
    $routePath = $query[0];
}else{
    $path = explode("/", $uri);
}
$folder = $path[1];
$page = array_key_exists($routePath, $routes) ? DIRECTORY_SEPARATOR . $routes[$routePath] : "/index";
echo "<div class='my-5'>";
include ROOT_PATH . '/public/views/' . $folder . $page . '.php';
echo "</div>"
?>

</body>
</html>