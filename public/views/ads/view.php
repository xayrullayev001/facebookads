<?php


use FacebookAds\Object\Ad;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\TargetingFields;
use helpers\StringHelper;

global $api;

$fields = [
    AdFields::ID,
    AdFields::NAME,
    AdFields::STATUS,
    AdFields::RECOMMENDATIONS,
    AdFields::TARGETING,
    AdFields::EFFECTIVE_STATUS,
    AdFields::BID_AMOUNT
];
$params = array(
    AdFields::ID => $_GET['id']
);
$ad = (new Ad($_GET['id']))->getSelf(
    $fields,
    $params
);


?>


<div class="container">
    <h2>This is campaign view <?= $_GET['id'] ?></h2>
    <a class="btn btn-success" href="/ads/update?id=<?= $ad->{AdSetFields::ID}; ?>">Update</a>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Field name</th>
            <th scope="col">Field value</th>
        </tr>
        </thead>
        <tbody>
        <?php $idx = 0;
        foreach ($fields as $field) { ?>
            <tr>
                <th scope="row"><?= ++$idx ?></th>
                <?php echo "<td>" . StringHelper::underscoreToCamelCase($field) . "</td>";
                if ($field === AdFields::RECOMMENDATIONS && !is_null($ad->{AdFields::RECOMMENDATIONS})) {
                    $recommendation = $ad->{$field};
                    echo "<td> ";
                    for ($i = 0; $i < count($recommendation); $i++) {
                        echo " <span>" . $recommendation[$i]['title'] . " <i class='bi bi-info-circle-fill' data-bs-toggle='tooltip' data-bs-placement='right'  title='" . $recommendation[$i]['message'] . "'  /> </span>";
                    }
                    echo "</td>";
                    continue;
                }
                if ($field == AdFields::TARGETING && !is_null($ad->{$field})) {
                    echo "<td>";

                    echo StringHelper::underscoreToCamelCase(TargetingFields::AGE_MAX) . " : " . $ad->{$field}[TargetingFields::AGE_MAX] . "<br />";
                    echo StringHelper::underscoreToCamelCase(TargetingFields::AGE_MIN) . " : " . $ad->{$field}[TargetingFields::AGE_MIN] . "<br />";
                    echo StringHelper::underscoreToCamelCase(TargetingFields::GEO_LOCATIONS) . " : ";
                    $loc = $ad->{$field}[TargetingFields::GEO_LOCATIONS]['countries'];
                    for ($it = 0; $it < count($loc) - 1; $it++) {
                        echo ' <span class="badge bg-success">' . $loc[$it] . ' </span>';
                    }
                    echo "</td>";
                    continue;
                }
                ?>
                <td><?= is_null($ad->{$field}) ? " Not installed " : StringHelper::underscoreToCamelCase($ad->{$field}); ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>


