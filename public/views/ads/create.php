<?php

use FacebookAds\Http\Exception\AuthorizationException;
use FacebookAds\Http\Exception\RequestException;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\AdCreative;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdCreativeFields;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Values\AdSetStatusValues;

/**
 * @var $campaigns Campaign
 * @var $adSets AdSet
 * @var $adCreatives AdCreative
 */

global $api;
$account = new AdAccount($_SESSION['user'][AdAccountFields::ID]);

if (isset($_POST['Ad'])) {
    try {
        $params = [
            AdFields::NAME => $_POST["Ad"][AdFields::NAME],
            AdFields::ADSET_ID => (int)$_POST["Ad"][AdFields::ADSET_ID],
            AdFields::CREATIVE => ['creative_id' => (int)$_POST["Ad"][AdFields::CREATIVE]],
            AdFields::STATUS => "PAUSED"
        ];
        $fields = [AdFields::ID];
        $ad = $account->createAd($fields, $params);

        echo '<div class="alert alert-success container" role="alert"> Ad successfully created for view ad <a href="/ads/view' . $ad->{AdFields::ID} . '"> checkout this page</a></div>';
    } catch (RequestException $e) {
        echo '<div class="alert alert-danger container" role="alert"> ' . $e->getMessage() . ' <a href="/ads">checkout this page</a></div>';
    }
}

// get campaigns
try {
    $fields = array(CampaignFields::ID, CampaignFields::NAME);
    $params = array('effective_status' => array('ACTIVE', 'PAUSED'));
    $campaigns = $account->getCampaigns($fields, $params);
} catch (AuthorizationException $e) {
    echo '<div class="alert alert-danger container" role="alert"> ' . $e->get() . ' <a href="/campaigns">checkout this page</a></div>';
}

// get ad sets
try {
    $fields = [AdSetFields::ID, AdSetFields::NAME];
    $params = array(
        AdSetFields::EFFECTIVE_STATUS => [
            AdSetStatusValues::ACTIVE,
            AdSetStatusValues::PAUSED
        ]
    );
    $adSets = $account->getAdSets($fields, $params);
} catch (RequestException $e) {
    echo '<div class="alert alert-danger container" role="alert"> ' . $e->getMessage() . ' <a href="/campaigns">checkout this page</a></div>';
}

// get ad creative
try {
    $fields = array(AdCreativeFields::ID, AdCreativeFields::NAME);
    $params = [];
    $adCreatives = $account->getAdCreatives($fields, $params);
} catch (RequestException $e) {
    echo '<div class="alert alert-danger container" role="alert"> ' . $e->getMessage() . ' <a href="/campaigns">checkout this page</a></div>';
}
?>

<div class="container">
    <h5>Before you create an <a href="/ads">ad</a>, you need an existing <a href="/adsets">ad set</a> and <a
                href="/adcreative">ad creative</a>.</h5>
    <form action="/ads/create" method="post">
        <div class="mb-3">
            <label for="adset" class="form-label">Select Ad set</label>
            <select name="Ad[<?= AdFields::ADSET_ID ?>]" class="form-select"
                    aria-label="Default select example">
                <option selected>Select ad sets</option>
                <?php
                foreach ($adSets as $adSet) {
                    echo '<option value="' . $adSet->{AdSetFields::ID} . '">' . $adSet->{AdSetFields::NAME} . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="mb-3">
            <label for="adname" class="form-label">Ad name</label>
            <input type="text" name="Ad[<?= AdFields::NAME ?>]" class="form-control" id="adname">
        </div>
        <div class="mb-3">
            <label for="adcreative" class="form-label">Select Ad creative</label>
            <select class="form-select" name="Ad[<?= AdFields::CREATIVE ?>]"
                    aria-label="Default select example">
                <?php
                foreach ($adCreatives as $creative) {
                    echo '<option value="' . $creative->{AdSetFields::ID} . '">' . $creative->{AdSetFields::NAME} . '</option>';
                }
                ?>
            </select>
        </div>
        <button type="submit" class="btn btn-primary my-5">Submit</button>

    </form>
</div>

