<?php


global $api;

use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Fields\TargetingFields;
use helpers\StringHelper;

global $api;
$account = new AdAccount($_SESSION['user'][AdAccountFields::ID]);

$fields = [
    AdFields::ID,
    AdFields::NAME,
    AdFields::STATUS,
    AdFields::RECOMMENDATIONS,
    AdFields::TARGETING,
    AdFields::EFFECTIVE_STATUS,
    AdFields::BID_AMOUNT
];
$params = [];
$ads = $account->getAds($fields, $params);
?>


<div class="container">
    <table class="table caption-top">
        <caption>List of Ads</caption>
        <a href="/ads/create" class="btn btn-primary">Create ad</a>
        <thead>
        <tr>
            <th scope="col">#</th>
            <?php foreach ($fields as $field) { ?>
                <th scope="col"><?= StringHelper::underscoreToCamelCase($field); ?></th>
            <?php } ?>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php $idx = 0;
        foreach ($ads as $ad) { ?>
            <tr>
                <td>  <?= ++$idx; ?> </td>
                <?php foreach ($fields as $field) {
                    if ($field === AdFields::RECOMMENDATIONS && !is_null($ad->{AdFields::RECOMMENDATIONS})) {
                        $recommendation = $ad->{$field};
                        for ($i = 0; $i < count($recommendation); $i++) {
                            echo "<td> <span>" . $recommendation[$i]['title'] . " <i class='bi bi-info-circle-fill' data-bs-toggle='tooltip' data-bs-placement='right'  title='" . $recommendation[$i]['message'] . "'  /> </span></td>";
                        }
                        continue;
                    }
                    if ($field == AdFields::TARGETING && !is_null($ad->{$field})) {
                        echo "<td>";

                        echo StringHelper::underscoreToCamelCase(TargetingFields::AGE_MAX) . " : " . $ad->{$field}[TargetingFields::AGE_MAX] . "<br />";
                        echo StringHelper::underscoreToCamelCase(TargetingFields::AGE_MIN) . " : " . $ad->{$field}[TargetingFields::AGE_MIN] . "<br />";
                        echo StringHelper::underscoreToCamelCase(TargetingFields::GEO_LOCATIONS) . " : ";
                        $loc = $ad->{$field}[TargetingFields::GEO_LOCATIONS]['countries'];
                        for ($it = 0; $it < count($loc) - 1; $it++) {
                            echo ' <span class="badge bg-success">' . $loc[$it] . ' </span>';
                        }
                        echo "</td>";
                        continue;
                    }
                    ?>
                    <td><?= is_null($ad->{$field}) ? " Not installed " : StringHelper::underscoreToCamelCase($ad->{$field}); ?></td>
                <?php } ?>
                <td>
                    <a href="/ads/view?id=<?= $ad->id ?>" class="btn btn-primary">View</a>
                    <a href="/ads/update?id=<?= $ad->id ?>" class="btn btn-warning">Update</a>
                    <a href="/ads/delete?id=<?= $ad->id ?>" class="btn btn-danger">Delete</a>
                </td>
            </tr>
        <?php } ?>

        </tbody>
    </table>
</div>