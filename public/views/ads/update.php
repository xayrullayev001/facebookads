<?php

use FacebookAds\Http\Exception\RequestException;
use FacebookAds\Object\Ad;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\AdCreative;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdFields;

/**
 * @var $campaigns Campaign
 * @var $adSets AdSet
 * @var $adCreatives AdCreative
 */

global $api;
$account = new AdAccount($_SESSION['user'][AdAccountFields::ID]);
$ad = new Ad($_GET['id'], null, $api);
$adSelf = $ad->getSelf([AdFields::ID, AdFields::NAME]);
if (isset($_POST['Ad'])) {
    try {
        $params = [
            AdFields::NAME => $_POST["Ad"][AdFields::NAME],
        ];
        $fields = [AdFields::ID];
        $ad->updateSelf($fields, $params);

        echo '<div class="alert alert-success container" role="alert"> Ad successfully created for view ad <a href="/ads/view?id=' . $ad->{AdFields::ID} . '"> checkout this page</a></div>';
    } catch (RequestException $e) {
        echo '<div class="alert alert-danger container" role="alert"> ' . $e->getMessage() . ' <a href="/ads">checkout this page</a></div>';
    }
}

?>

<div class="container">
    <h5>Before you create an <a href="/ads">ad</a>, you need an existing <a href="/adsets">ad set</a> and <a
                href="/adcreative">ad creative</a>.</h5>
    <form action="/ads/update?id=<?= $_GET['id']; ?>" method="post">
        <div class="mb-3">
            <label for="adname" class="form-label">Ad name</label>
            <input type="text" name="Ad[<?= AdFields::NAME ?>]" value="<?= $adSelf->{AdFields::NAME}; ?>"
                   class="form-control" id="adname">
        </div>

        <button type="submit" class="btn btn-primary my-5">Submit</button>

    </form>
</div>

