<?php

use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdCreativeFields;

global $api;


$fields = array(
    AdCreativeFields::ID,
    AdCreativeFields::NAME,
    AdCreativeFields::STATUS,
    AdCreativeFields::ADLABELS,
    AdCreativeFields::BODY,
    AdCreativeFields::ACTOR_ID,
    AdCreativeFields::IMAGE_FILE,
    AdCreativeFields::IMAGE_CROPS,
    AdCreativeFields::IMAGE_HASH,
    AdCreativeFields::THUMBNAIL_URL,
    AdCreativeFields::IMAGE_URL,
);
$params = array(
    'thumbnail_width' => 150,
    'thumbnail_height' => 120,
);

$adCreatives = (new AdAccount($_SESSION['user'][AdAccountFields::ID]))->getAdCreatives(
    $fields,
    $params
);

?>

<div class="container">
    <table class="table caption-top">
        <caption>List of Ad creatives</caption>
        <a href="/adcreative/create" class="btn btn-primary">Create Ad Creative</a>
        <thead>
        <tr>
            <th scope="col">#</th>
            <?php foreach ($fields as $field) { ?>
                <th scope="col"><?= ucfirst(str_replace("_", " ", $field)); ?></th>
            <?php } ?>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php $idx = 0;
        foreach ($adCreatives as $adCreative) { ?>
            <tr>
                <th scope="row"><?= ++$idx ?></th>
                <?php foreach ($fields as $field) {
                    if (in_array($field, [AdCreativeFields::THUMBNAIL_URL, AdCreativeFields::IMAGE_URL])) {
                        echo "<td><a target='_blank' href='" . $adCreative->{$field} . "'><img class='img-thumbnail' src='" . $adCreative->{$field} . "'  /></a></td>";
                        continue;
                    }
                    ?>

                    <td><?= $adCreative->{$field} ?></td>
                <?php } ?>
                <td>
                    <a href="/adcreative/view?id=<?= $adCreative->id ?>" class="btn btn-primary">View</a>
                    <a href="/adcreative/update?id=<?= $adCreative->id ?>" class="btn btn-warning">Update</a>
                    <a href="/adcreative/delete?id=<?= $adCreative->id ?>" class="btn btn-danger">Delete</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
