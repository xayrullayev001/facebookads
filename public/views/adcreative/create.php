<?php

use FacebookAds\Object\AdAccount;
use FacebookAds\Object\AdCreativeLinkData;
use FacebookAds\Object\AdCreativeObjectStorySpec;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdCreativeFields;
use FacebookAds\Object\Fields\AdCreativeLinkDataFields;
use FacebookAds\Object\Fields\AdCreativeObjectStorySpecFields;
use FacebookAds\Object\Values\AdCreativeCallToActionTypeValues;

global $api;

if (isset($_POST['Ad'])) {
    $post = [
        "AdCreative" => $_POST["Ad"]["AdCreative"],
        "AdCreativeObjectStorySpec" => $_POST["Ad"]["AdCreativeObjectStorySpec"],
        "AdCreativeLinkData" => $_POST["Ad"]['AdCreativeLinkData'],
    ];

    $link_data = new AdCreativeLinkData();
    $link_data->setData([
        AdCreativeLinkDataFields::MESSAGE => $post["AdCreativeLinkData"][AdCreativeLinkDataFields::MESSAGE],
        AdCreativeLinkDataFields::LINK => $post["AdCreativeLinkData"][AdCreativeLinkDataFields::LINK],
        AdCreativeLinkDataFields::CALL_TO_ACTION => [
            'type' => $post["AdCreativeLinkData"][AdCreativeLinkDataFields::CALL_TO_ACTION]["AdCreativeCallToActionType"],
            'value' => [
                'link' => $post["AdCreativeLinkData"][AdCreativeLinkDataFields::CALL_TO_ACTION]['url'],
            ]
        ]
    ]);


    $object_story_spec = new AdCreativeObjectStorySpec();
    $object_story_spec->setData(array(
        AdCreativeObjectStorySpecFields::PAGE_ID => $post['AdCreativeObjectStorySpec'][AdCreativeObjectStorySpecFields::PAGE_ID],
        AdCreativeObjectStorySpecFields::INSTAGRAM_ACTOR_ID => $post['AdCreativeObjectStorySpec'][AdCreativeObjectStorySpecFields::INSTAGRAM_ACTOR_ID],
        AdCreativeObjectStorySpecFields::LINK_DATA => $link_data,
    ));


    $params = [
        AdCreativeFields::NAME => $post["AdCreative"][AdCreativeFields::NAME],
        AdCreativeFields::OBJECT_STORY_SPEC => $object_story_spec,
    ];

//
//    $params = array(
//        'thumbnail_width' => 150,
//        'thumbnail_height' => 120,
//    );

    $fields = array(
        AdCreativeFields::THUMBNAIL_URL,
        AdCreativeFields::ID,
//        AdCreativeFields::NAME,
//        AdCreativeFields::STATUS,
//        AdCreativeFields::ADLABELS,
//        AdCreativeFields::BODY,
//        AdCreativeFields::ACTOR_ID,
//        AdCreativeFields::IMAGE_FILE,
//        AdCreativeFields::IMAGE_CROPS,
//        AdCreativeFields::IMAGE_HASH,
//        AdCreativeFields::IMAGE_URL,
    );
    try {
        $adCreatives = (new AdAccount($_SESSION['user'][AdAccountFields::ID]))->createAdCreative(
            $fields,
            $params
        );
        echo '<div class="alert alert-success container" role="alert">AdCreative successfully created for view <a href="/adcreative/view?id=' . $adCreatives->{AdCreativeFields::ID} . '"> checkout this page</a></div>';
    } catch (Exception $e) {
        echo '<div class="alert alert-danger container" role="alert">' . $e->getMessage() . ' <a href="/adcreative">Back to home</a> </div>';

    }
}


?>
<div class="container">

    <form action="/adcreative/create" method="post">
        <div class="accordion" id="accordionExample">
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingThree">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                        Ad Creative
                    </button>
                </h2>
                <div id="collapseThree" class="accordion-collapse collapse show" aria-labelledby="headingThree"
                     data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <strong>Ad Name</strong>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci amet architecto, assumenda
                            consequuntur cupiditate, deleniti exercitationem magni omnis placeat quasi rem tenetur.
                            Dolore impedit, tenetur!</p>
                        <p>
                            <a target="_blank"
                               href="https://www.facebook.com/ads/manager/account_settings/account_name_template/?act=810619469873809&pid=p1&page=account_settings&tab=account_name_template">Create
                                Name Template</a></p>
                        <div class="mb-3">
                            <label for="<?= AdCreativeFields::NAME ?>"
                                   class=" form-label"><?= ucfirst(str_replace("_", " ", AdCreativeFields::NAME)) ?></label>
                            <input type="text"
                                   name="<?= "Ad[AdCreative][" . AdCreativeFields::NAME . "]" ?>"
                                   class="form-control"
                                   id="<?= AdCreativeFields::NAME ?>" aria-describedby=" emailHelp">
                        </div>

                    </div>

                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="headingTwo">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Ad Creative Object Story Spec
                    </button>
                </h2>
                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                     data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <strong>Identity</strong>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur, culpa excepturi
                            exercitationem facere mollitia voluptatibus!</p>
                        <div class="mb-3">
                            <label for="<?= AdCreativeObjectStorySpecFields::PAGE_ID ?>"
                                   class=" form-label"><?= ucfirst(str_replace("_", " ", AdCreativeObjectStorySpecFields::PAGE_ID)) ?></label>
                            <input type="text"
                                   name="<?= "Ad[AdCreativeObjectStorySpec][" . AdCreativeObjectStorySpecFields::PAGE_ID . "]" ?>"
                                   class="form-control" value="103324265290960"
                                   id="<?= AdCreativeObjectStorySpecFields::PAGE_ID ?>" aria-describedby="pageid">
                            <div id="pageid"><p>Your Facebook Page or Instagram account represents your business in ads.
                                    You can also Create a Facebook Page</p>
                                <p>For Ad Network manager <strong>103324265290960</strong></p></div>
                        </div>

                        <div class="mb-3">
                            <label for="<?= AdCreativeObjectStorySpecFields::INSTAGRAM_ACTOR_ID ?>"
                                   class=" form-label"><?= ucfirst(str_replace("_", " ", AdCreativeObjectStorySpecFields::INSTAGRAM_ACTOR_ID)) ?></label>
                            <input type="text"
                                   name="<?= "Ad[AdCreativeObjectStorySpec][" . AdCreativeObjectStorySpecFields::INSTAGRAM_ACTOR_ID . "]" ?>"
                                   class="form-control"
                                   id="<?= AdCreativeObjectStorySpecFields::INSTAGRAM_ACTOR_ID ?>"
                                   aria-describedby="INSTAGRAM_ACTOR_ID">
                            <div id="INSTAGRAM_ACTOR_ID">You can add instagram page</div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingOne">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                        Ad Creative Link Data
                    </button>
                </h2>
                <div id="collapseOne" class="accordion-collapse collapse " aria-labelledby="headingOne"
                     data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <strong>Lorem ipsum dolor sit amet.</strong>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aperiam asperiores atque
                            consequatur culpa dicta dolores doloribus eveniet expedita fugit harum impedit inventore
                            iure laudantium, molestias nihil nisi, quo temporibus, vero voluptatem. Inventore ipsum,
                            quam.</p>

                        <div class="mb-3">
                            <label for="<?= AdCreativeLinkDataFields::CALL_TO_ACTION ?>"
                                   class=" form-label">Call to action button types </label>
                            <!--                        call to action AdCreativeCallToActionTypeValues -->
                            <select class="form-select"
                                    name="<?= "Ad[AdCreativeLinkData][" . AdCreativeLinkDataFields::CALL_TO_ACTION . "]" . "[AdCreativeCallToActionType]" ?>"
                                    aria-label="Default select example">
                                <option selected>Select one of call to action type</option>
                                <?php foreach (AdCreativeCallToActionTypeValues::getInstance()->getValues() as $value) {
                                    echo "<option value='" . $value . "'>" . $value . "</option>";
                                } ?>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="<?= AdCreativeLinkDataFields::CALL_TO_ACTION ?>"
                                   class=" form-label">Call to action button endpoint url</label>
                            <input type="url"
                                   name="<?= "Ad[AdCreativeLinkData][" . AdCreativeLinkDataFields::CALL_TO_ACTION . "]" ?>[url]"
                                   class="form-control"
                                   id="<?= AdCreativeLinkDataFields::CALL_TO_ACTION ?>" aria-describedby="weblink">
                            <div id="weblink">Enter the URL for the web page you want people to visit.</div>
                        </div>


                        <div class="mb-3">
                            <label for="<?= AdCreativeLinkDataFields::MESSAGE ?>"
                                   class=" form-label"><?= ucfirst(str_replace("_", " ", AdCreativeLinkDataFields::MESSAGE)) ?></label>
                            <input type="text"
                                   name="<?= "Ad[AdCreativeLinkData][" . AdCreativeLinkDataFields::MESSAGE . "]" ?>"
                                   class="form-control"
                                   id="<?= AdCreativeLinkDataFields::MESSAGE ?> " aria-describedby=" emailHelp">
                        </div>

                        <div class="mb-3">
                            <label for="<?= AdCreativeLinkDataFields::LINK ?>"
                                   class=" form-label"><?= ucfirst(str_replace("_", " ", AdCreativeLinkDataFields::LINK)) ?>
                                for view </label>
                            <input type="text"
                                   name="<?= "Ad[AdCreativeLinkData][" . AdCreativeLinkDataFields::LINK . "]" ?>"
                                   class="form-control"
                                   id="<?= AdCreativeLinkDataFields::LINK ?> " aria-describedby="displaylink">
                            <div id="displaylink">Show a shortened link instead of your full website URL in some
                                placements. The link should go to the same domain as your website URL. The position of
                                the display link varies by placement.
                            </div>
                        </div>

                        <p class="text-center">
                            <button type="submit" class="btn btn-success my-5">Submit</button>
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </form>

</div>