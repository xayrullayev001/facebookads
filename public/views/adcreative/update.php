<?php

use FacebookAds\Object\AdCreative;
use FacebookAds\Object\Fields\AdCreativeFields;

global $api;

if (isset($_POST['AdCreative'])) {
    $creative = new AdCreative($_GET['id'], null, $api);

    try {
        $creative->updateSelf([
            AdCreativeFields::NAME,
            AdCreativeFields::ID,
        ], [
            AdCreativeFields::NAME => $_POST['AdCreative'][AdCreativeFields::NAME],
        ]);
        echo '<div class="alert alert-success container" role="alert">AdCreative successfully updated for view <a href="/adcreative/view?id=' . $creative->{AdCreativeFields::ID} . '"> checkout this page</a></div>';
    } catch (Exception $exception) {
        echo '<div class="alert alert-danger container" role="alert">' . $exception->getMessage() . ' <a href="/adcreative">Back to home</a> </div>';

    }
}

?>

<div class="container">
    <form action="/adcreative/update?id=<?= $_GET['id'] ?>" method="post">
        <div class="mb-3">
            <label for="<?= AdCreativeFields::NAME ?>"
                   class=" form-label"><?= ucfirst(str_replace("_", " ", AdCreativeFields::NAME)) ?></label>
            <input type="text"
                   name="<?= "AdCreative[" . AdCreativeFields::NAME . "]" ?>"
                   class="form-control"
                   id="<?= AdCreativeFields::NAME ?>" aria-describedby="nameHelp">
            <div id="nameHelp">The name of the creative in the creative library. This field takes a string of up to 100
                characters.
            </div>
        </div>
        <p class="text-center">
            <button type="submit" class="btn btn-success my-5">Submit</button>
        </p>
    </form>
</div>
