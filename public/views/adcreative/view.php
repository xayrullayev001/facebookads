<?php

use FacebookAds\Object\AdCreative;
use FacebookAds\Object\Fields\AdPreviewFields;
use FacebookAds\Object\Values\AdPreviewAdFormatValues;
use const FacebookAds\Http\Response;

global $api;


$params = array(
    AdPreviewFields::AD_FORMAT => (isset($_GET['ad_format'])) ? $_GET['ad_format'] : AdPreviewAdFormatValues::DESKTOP_FEED_STANDARD,
);
$fields = array(
    AdPreviewFields::BODY,
    AdPreviewFields::AD_FORMAT,
    AdPreviewFields::POST,
    AdPreviewFields::PRODUCT_ITEM_IDS,
);

$preview = (new AdCreative($_GET['id'], null, $api))->getPreviews(
    $fields,
    $params
);
$response = $preview->getLastResponse()->getBody();
$res = json_decode($preview->getLastResponse()->getBody());
$body = $res->data[0];
function toString(string $viewType): string
{
    return ucfirst(strtolower(str_replace("_", " ", $viewType)));
}

?>
<div class="container">
    <div class="col-6">
        <a href="" class="btn btn-primary" target="_blank"
           id="previewBtn">Preview <?= (isset($_GET['ad_format'])) ? toString($_GET['ad_format']) : ""; ?></a>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Preview action type</th>
                <th scope="col">Preview</th>
            </tr>
            </thead>
            <tbody>
            <?php $idx = 0;
            foreach (AdPreviewAdFormatValues::getInstance()->getValues() as $item) { ?>
                <tr>
                    <th scope="row"><?= ++$idx; ?></th>
                    <td><?= toString($item) ?></td>
                    <td><a href="http://fbad.lc/adcreative/view?id=<?= $_GET['id'] ?>&ad_format=<?= $item ?>">
                            Preview </a></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <?= $body->body; ?>
    <script>
        parent.document.getElementById("previewBtn").attributes[0].value = parent.document.getElementsByTagName("iframe")[0].attributes[0].value;
    </script>
</div>