<?php


use FacebookAds\Object\AdCreative;

global $api;

if (isset($_GET['id'])) {
    $creative = new AdCreative($_GET['id'], null, $api);

    try {
        $creative->deleteSelf();
        echo '<div class="alert alert-success container" role="alert">AdCreative successfully updated for view <a href="/adcreative"> checkout this page</a></div>';
    } catch (Exception $exception) {
        echo '<div class="alert alert-danger container" role="alert">' . $exception->getMessage() . ' <a href="/adcreative">Back to home</a> </div>';

    }
}

