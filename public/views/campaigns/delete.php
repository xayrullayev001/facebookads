<?php

use FacebookAds\Object\Campaign;

global $api;

$campaign = new Campaign($_GET['id'], null, $api);
try {
    $campaignResponse = $campaign->deleteSelf();
    $success = json_decode($campaignResponse->getBody(), true);
    if ($success) {
        echo '<div class="alert alert-success" role="alert">Campaign successfully deleted for view <a href="/campaigns">checkout this page</a></div>';
    } else {
        echo '<div class="alert alert-error" role="alert">Something went wrong <a href="/campaigns">checkout this page</a></div>';

    }
} catch (FacebookAds\Http\Exception\AuthorizationException $e) {
    echo '<div class="alert alert-error" role="alert">' . $e->getMessage() . ' <a href="/campaigns">Back to home</a></div>';
}

?>

<h2>This is campaign delete <?= $_GET['id'] ?></h2>
