<?php


use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Values\CampaignObjectiveValues;
use FacebookAds\Object\Values\CampaignSpecialAdCategoriesValues;
use FacebookAds\Object\Values\CampaignSpecialAdCategoryCountryValues;
use FacebookAds\Object\Values\CampaignStatusValues;

global $api;


$fields = [
    CampaignFields::ID,
    CampaignFields::NAME,
    CampaignFields::ACCOUNT_ID,
    CampaignFields::SPECIAL_AD_CATEGORY_COUNTRY,
    CampaignFields::SPEND_CAP,
    CampaignFields::OBJECTIVE,
    CampaignFields::STATUS,
    CampaignFields::UPDATED_TIME,
    CampaignFields::STOP_TIME,
    CampaignFields::SOURCE_CAMPAIGN_ID,
    CampaignFields::TOPLINE_ID,
    CampaignFields::RECOMMENDATIONS,
    CampaignFields::SPECIAL_AD_CATEGORIES
];

$campaign = new Campaign($_GET['id'], null, $api);

if (isset($_POST['Campaign'])) {
    $params = array(
        'name' => $_POST['Campaign'][CampaignFields::NAME],
        'objective' => $_POST['Campaign'][CampaignFields::OBJECTIVE],
        'status' => $_POST['Campaign'][CampaignFields::STATUS],
        'special_ad_categories' => [$_POST['Campaign'][CampaignFields::SPECIAL_AD_CATEGORIES]],
    );
    $campaignResponse = $campaign->updateSelf($fields, $params);
    echo '<div class="alert alert-success" role="alert">Campaign successfully updated for view <a href="/campaigns/view?id=' . $campaignResponse->{CampaignFields::ID} . '">checkout this page</a></div>';


} else {

    $params = array(
        'effective_status' => array('ACTIVE', 'PAUSED'),
    );
    $campaignResponse = $campaign->getSelf($fields, $params);
}


?>
<div class="container">
    <h2>This is campaign update <?= $_GET['id'] ?></h2>

    <form method="post" action="/campaigns/update?id=<?= $_GET['id']; ?>">
        <div class="mb-3">
            <label for="name" class="form-label">Company name</label>
            <input type="text" name="Campaign[<?= CampaignFields::NAME ?>]" class="form-control" id="name"
                   value="<?= $campaignResponse->{CampaignFields::NAME} ?>" aria-describedby="name">
        </div>
        <div class="mb-3">
            <select name="Campaign[<?= CampaignFields::STATUS ?>]" class="form-select"
                    aria-label="Default select example">
                <?php foreach (CampaignStatusValues::getInstance()->getValues() as $value) { ?>
                    <option value="<?= $value ?>" <?= ($value === $campaignResponse->{CampaignFields::STATUS}) ? "selected" : ""; ?> ><?= $value ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="mb-3">
            <select name="Campaign[<?= CampaignFields::OBJECTIVE ?>]" class="form-select"
                    aria-label="Default select example">

                <?php foreach (CampaignObjectiveValues::getInstance()->getValues() as $value) { ?>
                    <option value="<?= $value ?>" <?= ($value === $campaignResponse->{CampaignFields::OBJECTIVE}) ? "selected" : ""; ?> ><?= $value ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="mb-3">
            <select name="Campaign[<?= CampaignFields::SPECIAL_AD_CATEGORIES ?>]" class="form-select"
                    aria-label="Default select example">
                <?php foreach (CampaignSpecialAdCategoriesValues::getInstance()->getValues() as $value) { ?>
                    <option value="<?= $value ?>" <?= ($value === $campaignResponse->{CampaignFields::SPECIAL_AD_CATEGORIES}) ? "selected" : ""; ?> ><?= $value ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="mb-3">
            <select name="Campaign[<?= CampaignFields::SPECIAL_AD_CATEGORY_COUNTRY ?>]" class="form-select"
                    aria-label="Default select example">
                <?php foreach (CampaignSpecialAdCategoryCountryValues::getInstance()->getValues() as $value) { ?>
                    <option value="<?= $value ?>" <?= ($value === $campaignResponse->{CampaignFields::SPECIAL_AD_CATEGORY_COUNTRY}) ? "selected" : ""; ?> ><?= $value ?></option>
                <?php } ?>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>