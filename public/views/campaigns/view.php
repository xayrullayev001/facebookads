<?php


use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;

$fields = [
    CampaignFields::ID,
    CampaignFields::NAME,
    CampaignFields::ACCOUNT_ID,
    CampaignFields::SPECIAL_AD_CATEGORY_COUNTRY,
    CampaignFields::SPEND_CAP,
    CampaignFields::OBJECTIVE,
    CampaignFields::STATUS,
    CampaignFields::UPDATED_TIME,
    CampaignFields::STOP_TIME,
    CampaignFields::SOURCE_CAMPAIGN_ID,
    CampaignFields::TOPLINE_ID,
    CampaignFields::RECOMMENDATIONS,
];

$params = array(
    'effective_status' => array('ACTIVE', 'PAUSED'),
);
global $api;

$campaign = new Campaign($_GET['id'], null, $api);
$campaignResponse = $campaign->getSelf($fields, $params);

?>
<div class="container">
    <h2>This is campaign view <?= $_GET['id'] ?></h2>
    <a class="btn btn-success" href="/campaigns/update?id=<?= $campaignResponse->{CampaignFields::ID} ?>">Update</a>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Field name</th>
            <th scope="col">Field value</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($fields as $field) { ?>
            <tr>
                <th scope="row">1</th>
                <td><?= $field ?></td>
                <td><?= $campaignResponse->{$field} ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

