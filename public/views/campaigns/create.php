<?php


use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Values\CampaignObjectiveValues;
use FacebookAds\Object\Values\CampaignSpecialAdCategoriesValues;
use FacebookAds\Object\Values\CampaignSpecialAdCategoryCountryValues;
use FacebookAds\Object\Values\CampaignStatusValues;

if (isset($_POST['Campaign'])) {
    global $api;


    $account = new AdAccount($_SESSION['user'][AdAccountFields::ID], null, $api);
    $fields = array();
    $params = array(
        'name' => $_POST['Campaign'][CampaignFields::NAME],
        'objective' => $_POST['Campaign'][CampaignFields::OBJECTIVE],
        'status' => $_POST['Campaign'][CampaignFields::STATUS],
        'special_ad_categories' => [$_POST['Campaign'][CampaignFields::SPECIAL_AD_CATEGORIES]],
    );
    $response = $account->createCampaign($fields, $params);

    echo '<div class="alert alert-success" role="alert">Campaign successfully created for view <a href="/campaigns/view?id=' . $response->{CampaignFields::ID} . '">checkout this page</a></div>';
}
?>
<div class="container">

    <h2>This is campaign create page</h2>
    <form method="post" action="/campaigns/create">
        <div class="mb-3">
            <label for="name" class="form-label">Company name</label>
            <input type="text" name="Campaign[<?= CampaignFields::NAME ?>]" class="form-control" id="name"
                   aria-describedby="name">
            <div id="name" class="form-text">We'll never share your email with anyone else.</div>
        </div>
        <div class="mb-3">
            <select name="Campaign[<?= CampaignFields::STATUS ?>]" class="form-select"
                    aria-label="Default select example">
                <option selected>Select campaign status</option>
                <?php foreach (CampaignStatusValues::getInstance()->getValues() as $value) {
                    echo '<option value="' . $value . '">' . $value . '</option>';
                } ?>
            </select>
        </div>
        <div class="mb-3">
            <select name="Campaign[<?= CampaignFields::OBJECTIVE ?>]" class="form-select"
                    aria-label="Default select example">
                <option selected>Select campaign objective</option>
                <?php foreach (CampaignObjectiveValues::getInstance()->getValues() as $value) {
                    echo '<option value="' . $value . '">' . $value . '</option>';
                } ?>
            </select>
        </div>
        <div class="mb-3">
            <select name="Campaign[<?= CampaignFields::SPECIAL_AD_CATEGORIES ?>]" class="form-select"
                    aria-label="Default select example">
                <option selected>Select campaign special ad categories</option>
                <?php foreach (CampaignSpecialAdCategoriesValues::getInstance()->getValues() as $value) {
                    echo '<option value="' . $value . '">' . $value . '</option>';
                } ?>
            </select>
        </div>
        <div class="mb-3">
            <select name="Campaign[<?= CampaignFields::SPECIAL_AD_CATEGORY_COUNTRY ?>]" class="form-select"
                    aria-label="Default select example">
                <option selected>Select campaign special ad categories</option>
                <?php foreach (CampaignSpecialAdCategoryCountryValues::getInstance()->getValues() as $value) {
                    echo '<option   value="' . $value . '">' . $value . '</option>';
                } ?>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>