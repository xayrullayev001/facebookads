<?php

use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\CampaignFields;

global $api;


$fields = array(
    CampaignFields::NAME,
    CampaignFields::ADLABELS,
    CampaignFields::DAILY_BUDGET,
    CampaignFields::BID_STRATEGY,
    CampaignFields::CAN_USE_SPEND_CAP,
    CampaignFields::ACCOUNT_ID
);
$params = array(
    'effective_status' => array('ACTIVE', 'PAUSED'),
);

$campaigns = (new AdAccount($_SESSION['user'][AdAccountFields::ID]))->getCampaigns(
    $fields,
    $params
);

?>

<div class="container">
    <table class="table caption-top">
        <caption>List of Campaigns</caption>
        <a href="/campaigns/create" class="btn btn-primary">Create Campaign</a>
        <thead>
        <tr>
            <th scope="col">#</th>
            <?php foreach ($fields as $field) { ?>
                <th scope="col"><?= ucfirst(str_replace("_", " ", $field)); ?></th>
            <?php } ?>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php $idx = 0;
        foreach ($campaigns as $campaign) { ?>
            <tr>
                <th scope="row"><?= ++$idx ?></th>
                <?php foreach ($fields as $field) { ?>
                    <td><?= $campaign->{$field} ?></td>
                <?php } ?>
                <td>
                    <a href="/campaigns/view?id=<?= $campaign->id ?>" class="btn btn-primary">View</a>
                    <a href="/campaigns/update?id=<?= $campaign->id ?>" class="btn btn-warning">Update</a>
                    <a href="/campaigns/delete?id=<?= $campaign->id ?>" class="btn btn-danger">Delete</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
