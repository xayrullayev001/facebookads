<?php

use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\User;

//User account
$user = new User(CONFIG['account_id']);

/**
 * all read fields from Campaigns need only change this array to publish on table
 */
$fields = array(
    AdAccountFields::ID,
    AdAccountFields::ACCOUNT_ID,
    AdAccountFields::NAME,
    AdAccountFields::BALANCE,
);
$accounts = $user->getAdAccounts($fields);
?>
<div class="container">
    <table class="table caption-top">
        <caption>List of Users</caption>
        <thead>
        <tr>
            <th scope="col">#</th>
            <?php foreach ($fields as $field) { ?>
                <th scope="col"><?= ucfirst(str_replace("_", " ", $field)); ?></th>
            <?php } ?>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php $idx = 0;
        foreach ($accounts as $account) { ?>
            <tr>
                <th scope="row"><?= ++$idx ?></th>
                <?php foreach ($fields as $field) { ?>
                    <td><?= $account->{$field} ?></td>
                <?php } ?>
                <td>
                    <button type="button" class="btn btn-primary">View</button>
                    <button type="button" class="btn btn-warning">Update</button>
                    <button type="button" class="btn btn-danger">Delete</button>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
