<?php

use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\TargetingFields;
use FacebookAds\Object\Values\AdSetStatusValues;
use helpers\StringHelper;

global $api;

$fields = [
    AdSetFields::ID,
    AdSetFields::NAME,
    AdSetFields::STATUS,
    AdSetFields::INSTAGRAM_ACTOR_ID,
    AdSetFields::RECOMMENDATIONS,
    AdSetFields::TARGETING,
    AdSetFields::DAILY_BUDGET,
    AdSetFields::EFFECTIVE_STATUS,
    AdSetFields::BID_AMOUNT,
    AdSetFields::BILLING_EVENT
];
$params = array(
    AdSetFields::EFFECTIVE_STATUS => [
        AdSetStatusValues::ACTIVE,
        AdSetStatusValues::PAUSED
    ]
);

$ads = (new AdAccount($_SESSION['user'][AdSetFields::ID]))->getAdSets(
    $fields,
    $params
);

?>


<div class="container">
    <table class="table caption-top">
        <caption>List of Ad sets</caption>
        <a href="/adsets/create" class="btn btn-primary">Create Ad set</a>
        <thead>
        <tr>
            <th scope="col">#</th>
            <?php foreach ($fields as $field) { ?>
                <th scope="col"><?= StringHelper::underscoreToCamelCase($field); ?></th>
            <?php } ?>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php $idx = 0;
        foreach ($ads as $adSet) { ?>
            <tr>
                <th scope="row"><?= ++$idx ?></th>
                <?php foreach ($fields as $field) {
                    if ($field == AdSetFields::RECOMMENDATIONS && is_array($adSet->{$field})) {
                        echo " <td>";
                        $itemIdx = 0;
                        foreach ($adSet->{$field} as $r) {
                            echo "<span class='badge bg-warning text-dark' >" . ++$itemIdx . " ) " . $r['title'] . " <i class='bi bi-info-circle-fill' data-bs-toggle='tooltip' data-bs-placement='right' title='" . $r['message'] . "' ></i> </span>";
                        }
                        echo "</td>";
                        continue;
                    }
                    if ($field == AdSetFields::TARGETING && is_array($adSet->{$field})) {
                        echo " <td>";
                        $itemIdx = 0;
                        $item = $adSet->{$field};
                        echo StringHelper::underscoreToCamelCase(TargetingFields::AGE_MAX) . " <span> " . $item[TargetingFields::AGE_MAX] . "</span><br />";
                        echo StringHelper::underscoreToCamelCase(TargetingFields::AGE_MIN) . " <span> " . $item[TargetingFields::AGE_MIN] . "</span><br />";
                        echo StringHelper::underscoreToCamelCase(TargetingFields::GEO_LOCATIONS) . " <span> " . implode(", ", $item[TargetingFields::GEO_LOCATIONS]['location_types']) . "</span><br />";
                        echo StringHelper::underscoreToCamelCase(TargetingFields::GEO_LOCATIONS) . " <span> " . implode(", ", $item[TargetingFields::GEO_LOCATIONS][TargetingFields::COUNTRIES]) . "</span><br />";
                        if (isset($item[TargetingFields::FACEBOOK_POSITIONS])) echo StringHelper::underscoreToCamelCase(TargetingFields::FACEBOOK_POSITIONS) . " <span> " . implode(", ", $item[TargetingFields::FACEBOOK_POSITIONS]) . "</span><br />";
                        echo "</td>";
                        continue;
                    }

                    ?>
                    <td><?= StringHelper::underscoreToCamelCase($adSet->{$field}); ?></td>
                <?php } ?>
                <td>
                    <a href="/adsets/view?id=<?= $adSet->id ?>" class="btn btn-primary">View</a>
                    <a href="/adsets/update?id=<?= $adSet->id ?>" class="btn btn-warning">Update</a>
                    <a href="/adsets/delete?id=<?= $adSet->id ?>" class="btn btn-danger">Delete</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
