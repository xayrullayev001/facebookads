<?php


global $api;

use FacebookAds\Cursor;
use FacebookAds\Http\Exception\ClientException;
use FacebookAds\Http\Exception\RequestException;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\TargetingFields;
use helpers\StringHelper;

/**
 * @var $adSet Cursor
 * @var $idx int index of array
 */
$fields = [
    AdSetFields::ID,
    AdSetFields::NAME,
    AdSetFields::STATUS,
    AdSetFields::RECOMMENDATIONS,
    AdSetFields::TARGETING,
    AdSetFields::DAILY_BUDGET,
    AdSetFields::EFFECTIVE_STATUS,
    AdSetFields::BID_AMOUNT,
    AdSetFields::BILLING_EVENT,
    AdSetFields::BILLING_EVENT,
];


$params = array();
try {
    $adSet = (new AdAccount($_SESSION['user'][AdSetFields::ID]))->getAdSets(
        $fields,
        $params
    )->current()->exportAllData();
} catch (ClientException $e) {
    echo '<div class="alert alert-danger container" role="alert"> ' . $e->getErrorUserMessage() . ' <a href="/campaigns">checkout this page</a></div>';
} catch (RequestException $e) {
    echo '<div class="alert alert-danger container" role="alert"> ' . $e->getMessage() . ' <a href="/campaigns">checkout this page</a></div>';
}

$idx = 0;

//echo json_encode($adSet->current()->exportAllData());
?>

<div class="container">
    <h2>This is campaign view <?= $_GET['id'] ?></h2>
    <a class="btn btn-success" href="/adsets/update?id=<?= $adSet[AdSetFields::ID]; ?>">Update</a>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Field name</th>
            <th scope="col">Field value</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($fields as $field) { ?>
            <tr>
                <th scope="row"><?= ++$idx ?></th>
                <?php echo "<td>" . StringHelper::underscoreToCamelCase($field) . "</td>";
                if ($field == AdSetFields::RECOMMENDATIONS && is_array($adSet[$field])) {
                    echo " <td>";
                    $itemIdx = 0;
                    foreach ($adSet[$field] as $r) {
                        echo ++$itemIdx . ") <span >" . $r['title'] . " <i class='bi bi-info-circle-fill' data-bs-toggle='tooltip' data-bs-placement='right' title='" . $r['message'] . "' ></i>  </span>";
                    }
                    echo "</td>";
                    continue;
                }
                if ($field == AdSetFields::TARGETING && is_array($adSet[$field])) {
                    /**
                     * if you want all print uncomment this
                     *
                     * foreach (TargetingFields::getInstance()->getValues() as $targetField) {
                     *      echo"<td> ". $adSet[$field][$targetField] ." </td>";
                     * }
                     */
                    echo " <td>";
                    $item = $adSet[$field];
                    echo StringHelper::underscoreToCamelCase(TargetingFields::AGE_MAX) . " <span> " . $item[TargetingFields::AGE_MAX] . "</span><br />";
                    echo StringHelper::underscoreToCamelCase(TargetingFields::AGE_MIN) . " <span> " . $item[TargetingFields::AGE_MIN] . "</span><br />";
                    echo StringHelper::underscoreToCamelCase(TargetingFields::GEO_LOCATIONS) . " <span> " . implode(", ", $item[TargetingFields::GEO_LOCATIONS]['location_types']) . "</span><br />";
                    echo StringHelper::underscoreToCamelCase(TargetingFields::GEO_LOCATIONS) . " <span> " . implode(", ", $item[TargetingFields::GEO_LOCATIONS][TargetingFields::COUNTRIES]) . "</span><br />";
                    if (isset($item[TargetingFields::FACEBOOK_POSITIONS])) echo StringHelper::underscoreToCamelCase(TargetingFields::FACEBOOK_POSITIONS) . " <span> " . implode(", ", $item[TargetingFields::FACEBOOK_POSITIONS]) . "</span><br />";
                    echo "</td>";
                    continue;
                }
                ?>
                <td><?= is_null($adSet[$field]) ? " Not installed " : StringHelper::underscoreToCamelCase($adSet[$field]); ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

