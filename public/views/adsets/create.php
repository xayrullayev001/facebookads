<?php

use FacebookAds\Http\Exception\RequestException;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdSet;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Fields\TargetingFields;
use FacebookAds\Object\Targeting;
use FacebookAds\Object\Values\AdEffectiveStatusValues;
use FacebookAds\Object\Values\AdSetBillingEventValues;
use FacebookAds\Object\Values\AdSetDestinationTypeValues;
use FacebookAds\Object\Values\AdSetOptimizationGoalValues;
use FacebookAds\Object\Values\CampaignSpecialAdCategoryCountryValues;

/**
 * @var $campaigns Campaign[]
 * @var $targeting Targeting[]
 * @var $account AdAccount[]
 */
global $api;

//all campaigns for select
try {
    $campaigns = (new AdAccount($_SESSION['user'][AdAccountFields::ID]))->getCampaigns(
        [
            //campaign fields
            CampaignFields::ID,
            CampaignFields::NAME
        ],
        [
            // campaign params
            'effective_status' => [
                AdEffectiveStatusValues::ACTIVE,
                AdEffectiveStatusValues::PAUSED
            ]
        ]
    );

} catch (Exception $e) {
    echo '<div class="alert alert-danger container" role="alert"> ' . $e->getMessage() . ' <a href="/campaigns">checkout this page</a></div>';
}

if (isset($_POST['Ad'])) {
    // crate targeting
    $targeting = (new Targeting())->setData([
        TargetingFields::GEO_LOCATIONS => [
            'countries' => $_POST['Ad']['AdSet'][AdSetFields::TARGETING]['countries']
        ],

        TargetingFields::FACEBOOK_POSITIONS => ['feed'],
        TargetingFields::AGE_MAX => $_POST['Ad']['AdSet'][TargetingFields::AGE_MAX],
        TargetingFields::AGE_MIN => $_POST['Ad']['AdSet'][TargetingFields::AGE_MIN],
    ]);


    $fields = [
        AdSetFields::ID,
    ];
    $params = [
        AdSetFields::NAME => $_POST['Ad']['AdSet'][AdSetFields::NAME],
        AdSetFields::DAILY_BUDGET => $_POST['Ad']['AdSet'][AdSetFields::DAILY_BUDGET],
        AdSetFields::BID_AMOUNT => $_POST['Ad']['AdSet'][AdSetFields::BID_AMOUNT],
        AdSetFields::BILLING_EVENT => $_POST['Ad']['AdSet'][AdSetFields::BILLING_EVENT],
        AdSetFields::OPTIMIZATION_GOAL => $_POST['Ad']['AdSet'][AdSetFields::OPTIMIZATION_GOAL],
        AdSetFields::CAMPAIGN_ID => $_POST['Ad']['AdSet'][AdSetFields::CAMPAIGN_ID],
        AdSetFields::DESTINATION_TYPE => $_POST['Ad']['AdSet'][AdSetFields::DESTINATION_TYPE],
        AdSetFields::STATUS => $_POST['Ad']['AdSet'][AdSetFields::STATUS],
        AdSetFields::START_TIME => (new DateTime($_POST['Ad']['AdSet'][AdSetFields::START_TIME]))->format("Y-m-d H:i:s T"),
        AdSetFields::END_TIME => (new DateTime($_POST['Ad']['AdSet'][AdSetFields::END_TIME]))->format("Y-m-d H:i:s T"),
        AdSetFields::PROMOTED_OBJECT => array('page_id' => $_POST['Ad']['AdSet']['page_id']),//"103324265290960"
        AdSetFields::TARGETING => $targeting

    ];


    $account = new AdAccount($_SESSION['user'][AdSetFields::ID], null, $api);

    try {
        $response = $account->createAdSet($fields, $params);
        echo '<div class="alert alert-success container" role="alert">Ad set successfully created for view <a href="/adsets/view?id=' . $response->{AdSetFields::ID} . '">checkout this page</a></div>';

    } catch (RequestException $e) {
        echo '<div class="alert alert-danger container" role="alert"> ' . $e->getErrorUserMessage() . ' <a href="/campaigns">checkout this page</a></div>';

    }
}
?>

<div class="container">

    <h2>This is campaign create page</h2>
    <form action="/adsets/create" method="post">
        <div class="accordion" id="accordionExample">
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingOne">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseOne"
                            aria-expanded="true" aria-controls="collapseOne">
                        Ad Set Name
                    </button>
                </h2>
                <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"
                     data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <strong>Ad Set Name</strong>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci amet architecto, assumenda
                            consequuntur cupiditate, deleniti exercitationem magni omnis placeat quasi rem tenetur.
                            Dolore impedit, tenetur!</p>
                        <p>
                            <a target="_blank"
                               href="https://www.facebook.com/ads/manager/account_settings/account_name_template/?act=810619469873809&pid=p1&page=account_settings&tab=account_name_template">Create
                                Name Template</a></p>
                        <div class="mb-3">
                            <label for="<?= AdSetFields::NAME ?>"
                                   class=" form-label"><?= ucfirst(str_replace("_", " ", AdSetFields::NAME)) ?></label>
                            <input type="text"
                                   name="<?= "Ad[AdSet][" . AdSetFields::NAME . "]" ?>"
                                   class="form-control"
                                   id="<?= AdSetFields::NAME ?>" aria-describedby=" emailHelp">
                        </div>

                        <div class="mb-3">
                            <label for="page_id"
                                   class="form-label">Page id </label>
                            <input type="text"
                                   name="<?= "Ad[AdSet][page_id]" ?>"
                                   class="form-control" value="103324265290960"
                                   id="page_id" aria-describedby="page_id">
                            <div id="page_id">For Ad Network manager "103324265290960"</div>
                        </div>

                        <div class="mb-3">
                            <label for="<?= AdSetFields::CAMPAIGN_ID ?>"
                                   class=" form-label">Select campaign</label>
                            <select name="Ad[AdSet][<?= AdSetFields::CAMPAIGN_ID ?>]"
                                    id="<?= AdSetFields::CAMPAIGN_ID ?>" class="form-select"
                                    aria-label="Default select example">
                                <option selected>Select Campaign</option>
                                <?php foreach ($campaigns as $value) { ?>
                                    <option value='<?= $value->{CampaignFields::ID} ?>'> <?= $value->{CampaignFields::NAME} ?> </option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="mb-3">
                            <label for="<?= AdSetFields::DESTINATION_TYPE ?>"
                                   class=" form-label">Select <?= ucfirst(str_replace("_", " ", AdSetFields::DESTINATION_TYPE)) ?></label>
                            <select name="Ad[AdSet][<?= AdSetFields::DESTINATION_TYPE ?>]"
                                    id="<?= AdSetFields::DESTINATION_TYPE ?>" class="form-select"
                                    aria-label="Default select example">
                                <option selected>
                                    Select <?= ucfirst(str_replace("_", " ", AdSetFields::DESTINATION_TYPE)) ?></option>
                                <?php foreach (AdSetDestinationTypeValues::getInstance()->getValues() as $value) { ?>
                                    <option value='<?= $value ?>'> <?= $value ?> </option>
                                <?php } ?>
                            </select>
                        </div>

                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingTwo">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Budget & Schedule
                    </button>
                </h2>
                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                     data-bs-parent="#accordionExample">
                    <div class="accordion-body">


                        <h4>Schedule</h4>
                        <p>Schedule You can choose to run your ads continuously starting today or only during a specific
                            date range.
                            When using a lifetime campaign budget
                            Ad set schedules affect the distribution of a lifetime campaign budget. Days with more
                            opportunities receive more budget, so the amount spent per day will fluctuate.</p>
                        <div class="mb-3">
                            <label for="<?= AdSetFields::DAILY_BUDGET ?>"
                                   class=" form-label"><?= ucfirst(str_replace("_", " ", AdSetFields::DAILY_BUDGET)) ?></label>
                            <input type="text"
                                   name="<?= "Ad[AdSet][" . AdSetFields::DAILY_BUDGET . "]" ?>"
                                   class="form-control"
                                   id="<?= AdSetFields::DAILY_BUDGET ?>"
                                   aria-describedby="<?= AdSetFields::DAILY_BUDGET ?>">
                            <div id="<?= AdSetFields::DAILY_BUDGET ?>"> Your ad set budget is the daily or lifetime
                                amount you want to spend on this ad set.
                                A daily budget is the average you'll spend every day. A lifetime budget is the maximum
                                you'll
                                spend during the lifetime of your ad set.
                            </div>
                        </div>

                        <label class="form-label" for="<?= AdSetFields::START_TIME ?>">Start Date</label>
                        <div class="mb-3 input-group">
                            <span class="input-group-text"> <?= ucfirst(str_replace("_", " ", AdSetFields::START_TIME)) ?></span>
                            <input class="form-control" type="datetime-local" id="<?= AdSetFields::START_TIME ?>"
                                   name="Ad[AdSet][<?= AdSetFields::START_TIME ?>]">
                            <span class="input-group-text"> <?= ucfirst(str_replace("_", " ", AdSetFields::END_TIME)) ?></span>
                            <input class="form-control" type="datetime-local" id="<?= AdSetFields::END_TIME ?>"
                                   name="Ad[AdSet][<?= AdSetFields::END_TIME ?>]">
                        </div>

                        <div class="mb-3">
                            <label for="<?= AdSetFields::BID_AMOUNT ?>"
                                   class=" form-label"><?= ucfirst(str_replace("_", " ", AdSetFields::BID_AMOUNT)) ?></label>
                            <input type="text"
                                   name="<?= "Ad[AdSet][" . AdSetFields::BID_AMOUNT . "]" ?>"
                                   class="form-control"
                                   id="<?= AdSetFields::BID_AMOUNT ?>" aria-describedby=" emailHelp">
                        </div>
                        <div class="mb-3">
                            <label for="<?= AdSetFields::BILLING_EVENT ?>"
                                   class=" form-label"><?= ucfirst(str_replace("_", " ", AdSetFields::BILLING_EVENT)) ?></label>
                            <select name="<?= "Ad[AdSet][" . AdSetFields::BILLING_EVENT . "]" ?>"
                                    id="<?= AdSetFields::BILLING_EVENT ?>" class="form-select"
                                    aria-label="Default select example">
                                <option selected>Open this select menu</option>
                                <?php foreach (AdSetBillingEventValues::getInstance()->getValues() as $value) {
                                    echo "<option value='" . $value . "'>" . $value . "</option>";
                                } ?>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="<?= AdSetFields::OPTIMIZATION_GOAL ?>"
                                   class=" form-label"><?= ucfirst(str_replace("_", " ", AdSetFields::OPTIMIZATION_GOAL)) ?></label>
                            <select name="<?= "Ad[AdSet][" . AdSetFields::OPTIMIZATION_GOAL . "]" ?>"
                                    id="<?= AdSetFields::OPTIMIZATION_GOAL ?>" class="form-select"
                                    aria-label="Default select example">
                                <option selected>Open this select menu</option>
                                <?php foreach (AdSetOptimizationGoalValues::getInstance()->getValues() as $value) {
                                    echo "<option value='" . $value . "'>" . $value . "</option>";
                                } ?>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingThree">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Audience
                    </button>
                </h2>
                <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree"
                     data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        Define who you want to see your ads. <a
                                href="https://www.facebook.com/business/help/717368264947302?id=176276233019487">Learn
                            more</a>


                        <div id="location">Enter one or more global regions, countries, states/regions, cities, postal
                            codes, addresses or Designated Market Areas ® to show or exclude your ad to people in those
                            locations. Location targeting is not available in all countries.
                        </div>
                        <div class="mb-3">
                            <label for="<?= AdSetFields::TARGETING ?>"
                                   class=" form-label"><?= ucfirst(str_replace("_", " ", TargetingFields::GEO_LOCATIONS)) ?></label>
                            <select multiple aria-label="multiple select example"
                                    name="<?= "Ad[AdSet][" . AdSetFields::TARGETING . "][countries][]" ?>"
                                    id="<?= AdSetFields::TARGETING ?>" class="form-select"
                                    aria-label="Default select example">
                                <option selected>Select audience location</option>
                                <?php foreach (CampaignSpecialAdCategoryCountryValues::getInstance()->getValues() as $value) {
                                    echo "<option value='" . $value . "'>" . $value . "</option>";
                                } ?>
                            </select>
                        </div>
                        <label class="form-label" for="<?= TargetingFields::AGE_MAX ?>">Select the minimum and maximum
                            age of the people who will find your ad relevant.</label>
                        <div class="mb-3 input-group">
                            <span class="input-group-text"> <?= ucfirst(str_replace("_", " ", TargetingFields::AGE_MAX)) ?></span>
                            <input class="form-control" type="number" id="<?= TargetingFields::AGE_MAX ?>"
                                   name="Ad[AdSet][<?= TargetingFields::AGE_MAX ?>]">
                            <span class="input-group-text"> <?= ucfirst(str_replace("_", " ", TargetingFields::AGE_MIN)) ?></span>
                            <input class="form-control" type="number" id="<?= TargetingFields::AGE_MIN ?>"
                                   name="Ad[AdSet][<?= TargetingFields::AGE_MIN ?>]">
                        </div>

                        <div class="mb-3">
                            <label for="<?= AdSetFields::STATUS ?>"
                                   class=" form-label"><?= ucfirst(str_replace("_", " ", AdSetFields::STATUS)) ?></label>
                            <select name="<?= "Ad[AdSet][" . AdSetFields::STATUS . "]" ?>"
                                    id="<?= AdSetFields::STATUS ?>" class="form-select"
                                    aria-label="Default select example">
                                <option selected>Select audience location</option>
                                <option value='<?= AdEffectiveStatusValues::ACTIVE ?>'><?= AdEffectiveStatusValues::ACTIVE ?></option>
                                <option value='<?= AdEffectiveStatusValues::PAUSED ?>'> <?= AdEffectiveStatusValues::PAUSED ?></option>
                            </select>
                        </div>


                        <button class="btn btn-primary my-5">Submit</button>
                    </div>
                </div>
            </div>

        </div>
    </form>
</div>
