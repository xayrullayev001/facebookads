<?php

global $api;

use FacebookAds\Object\AdSet;

try {
    $adSet = new AdSet($_GET['id'], null, $api);
    if ($adSet->deleteSelf())
        echo '<div class="alert alert-success container" role="alert">AdSet successfully deleted. For continue <a href="/adsets">checkout this page</a></div>';

} catch (Exception $e) {
    echo '<div class="alert alert-error container" role="alert">' . $e->getMessage() . '  <a href="/adsets">Back to home page</a></div>';

}