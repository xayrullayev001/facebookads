<?php

use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdImageFields;

global $api;

try {
    $account = new AdAccount($_SESSION['user'][AdAccountFields::ID], null, $api);
    $account->deleteAdImages([], [AdImageFields::HASH => $_GET['hash']]);

    echo '<div class="alert alert-success" role="alert">AdImage successfully deleted. For continue <a href="/adimages">checkout this page</a></div>';
} catch (Exception $e) {
    echo '<div class="alert alert-error" role="alert">' . $e->getMessage() . '  <a href="/adimages">Back to home page</a></div>';

}
