<?php


use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdImageFields;

global $api;

if (isset($_POST['AdImage'])) {
    $target_file = ROOT_PATH . "/uploads/" . basename($_FILES['AdImage']['name'][AdImageFields::URL]);
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

    $tmpFile = $_FILES['AdImage']['tmp_name'][AdImageFields::URL];

    move_uploaded_file($tmpFile, $target_file);
    $fields = [
        AdImageFields::ID,
        AdImageFields::HASH,
        AdImageFields::ACCOUNT_ID,
    ];
    $params = [
        AdImageFields::ACCOUNT_ID => $_SESSION['user'][AdAccountFields::ID],
        AdImageFields::FILENAME => $target_file,
        AdImageFields::NAME => $_POST['AdImage'][AdImageFields::NAME],
    ];
    $account = new AdAccount($_SESSION['user'][AdAccountFields::ID], null, $api);
    try {
        $image = $account->createAdImage($fields, $params);
        echo '<div class="alert alert-success" role="alert">AdImage successfully created. For view <a href="/adimages">checkout this page</a></div>';

    } catch (Exception $e) {
        echo '<div class="alert alert-error" role="alert">' . $e->getMessage() . '  <a href="/adimages/create">Back to create new</a></div>';
    }
}
?>

<div class="container">
    <h3 class="mt-5">this is ad images</h3>
    <a href="/adimages/create" class="btn btn-success">Create images</a>
    <form action="/adimages/create" method="post" class="mt-5" enctype="multipart/form-data">
        <div class="mb-3">
            <label for="formFile" class="form-label">Select image from device. For example, provide an image file such
                as .bmp, .jpeg, or .gif:</label>
            <input class="form-control" type="file" name="AdImage[<?= AdImageFields::URL ?>]" id="formFile">
        </div>
        <div class="mb-3">
            <label for="name" class="form-label">Image name</label>
            <input type="text" class="form-control" name="AdImage[<?= AdImageFields::NAME ?>]" id="name">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>