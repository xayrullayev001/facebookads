<?php


use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdImageFields;

global $api;

$account = new AdAccount($_SESSION['user'][AdAccountFields::ID], null, $api);
$fields = [
    AdImageFields::ID,
    AdImageFields::FILENAME,
    AdImageFields::HASH,
    AdImageFields::NAME,
    AdImageFields::ACCOUNT_ID,
    AdImageFields::UPDATED_TIME,
    AdImageFields::STATUS,
    AdImageFields::CREATED_TIME,
    AdImageFields::BYTES,
    AdImageFields::URL,
];
$params = [

];
$images = $account->getAdImages($fields, $params);

?>

<div class="container">
    <h3>this is ad images</h3>
    <a href="/adimages/create" class="btn btn-success">Create images</a>

    <div class="container">
        <?php foreach ($images as $image) { ?>
            <div class="card mt-5 mb-25">
                <img src="<?= $image->{AdImageFields::URL} ?>" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text"><?= ucfirst(str_replace("_", " ", AdImageFields::NAME)) . ": " . $image->{AdImageFields::NAME} ?></p>
                    <p class="card-text"><?= ucfirst(str_replace("_", " ", AdImageFields::HASH)) . ": " . $image->{AdImageFields::HASH} ?></p>
                    <p class="card-text"><?= ucfirst(str_replace("_", " ", AdImageFields::CREATED_TIME)) . ": " . $image->{AdImageFields::CREATED_TIME} ?></p>
                    <p class="card-text"><?= ucfirst(str_replace("_", " ", AdImageFields::UPDATED_TIME)) . ": " . $image->{AdImageFields::UPDATED_TIME} ?></p>
                    <p class="card-text"><?= ucfirst(str_replace("_", " ", AdImageFields::ID)) . ": " . $image->{AdImageFields::ID} ?></p>
                </div>
                <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                    <a href="/adimages/view?hash=<?= $image->{AdImageFields::HASH} ?>" class="btn btn-success">View
                        image</a>
                    <a href="/adimages/delete?id=<?= (int)$image->{AdImageFields::ID} ?>&hash=<?= $image->{AdImageFields::HASH} ?>"
                       class="btn btn-danger">Delete</a>
                </div>
            </div>
        <?php } ?>
    </div>

</div>