<?php

use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
$fbConfig = require 'config/fb.config.php';

define("ROOT_PATH", dirname(__FILE__));
define("CONFIG", $fbConfig);

require ROOT_PATH . '/vendor/autoload.php';

$api = Api::init(CONFIG['app_id'], CONFIG['app_secret'], CONFIG['access_token']);
$api->setLogger(new CurlLogger());
$api->setDefaultGraphVersion("v10.0");
require ROOT_PATH . '/public/layouts/main.php';
